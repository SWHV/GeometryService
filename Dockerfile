FROM debian:bookworm

RUN apt-get update && apt-get -y upgrade && \
    apt-get -y install python3-dev python3-pip && \
    apt-get -y install wget git git-lfs cmake ninja-build && \
    apt-get -y autoremove && apt-get clean && apt-get autoclean

# ARG CACHE_DATE=1970-01-01
RUN git clone --depth=1 https://gitlab.com/SWHV/GeometryService.git
RUN cd /root && mkdir build && cd build && cmake /GeometryService -G Ninja && ninja install

EXPOSE 7789
CMD /geometry/python/bin/geometry_service
