from setuptools import setup

kwargs = {
    "name": "GeometryService",
    "description": "Solar System Geometry Service",
    "long_description": open("README.md").read(),
    "author": "SWHV ROB",
    "author_email": "swhv@oma.be",
    "url": "https://github.com/Helioviewer-Project/GeometryService",
    "packages": ["GeometryService"],
    "scripts": ["bin/geometry_service"],
    "license": "MIT",
}

instllrqrs = [
    "dill",
    "lxml",
    "msgpack-python",
    "spyne>=2.13.2-alpha",
    "sortedcontainers",
    "Werkzeug",
]
kwargs["install_requires"] = instllrqrs

clssfrs = [
    "Programming Language :: Python",
    "Programming Language :: Python :: 2.7",
    "Programming Language :: Python :: 3.7",
    "Programming Language :: Python :: Implementation :: CPython",
    "License :: OSI Approved :: MIT License",
    "Development Status :: 5 - Production/Stable",
    "Operating System :: POSIX",
    "Intended Audience :: Science/Research",
    "Intended Audience :: Information Technology",
    "Topic :: Software Development :: Libraries :: Python Modules",
]
kwargs["classifiers"] = clssfrs

kwargs["version"] = "0.7"

setup(**kwargs)
