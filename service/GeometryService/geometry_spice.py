import os
import spiceypy as sp


def _furnish(directory, kernel):
    name = os.path.join(directory, kernel)
    sp.unload(name)

    if not kernel.startswith("-"):
        sp.furnsh(name)


def init(dirs):
    sp.erract("set", 10, "return")
    sp.errdev("set", 10, "null")
    sp.trcoff()
    _furnish("${CMAKE_INSTALL_PREFIX}/data/generic", "meta.ker")

    kdirs = ["${CMAKE_INSTALL_PREFIX}/data/kernels"]
    if dirs and all(isinstance(s, str) for s in dirs):
        kdirs.extend(dirs)

    for dir in kdirs:
        for dirpath, dirnames, filenames in os.walk(dir, topdown=True):
            dirnames[:] = [d for d in dirnames if not d.startswith("-")]
            for f in filenames:
                _furnish(dirpath, f)
