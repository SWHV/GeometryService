KPL/FK

Extra IDs for STEREO

\begindata

    NAIF_BODY_CODE               += ( -234 )
    NAIF_BODY_NAME               += ( 'STEREO-A' )

    NAIF_BODY_CODE               += ( -235 )
    NAIF_BODY_NAME               += ( 'STEREO-B' )

\begintext
